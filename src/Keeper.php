<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba//alpaca
 */

namespace xibalba\alpaca;

use xibalba\tuza\DbException;
use xibalba\tuza\DbAware;

use xibalba\ocelote\Checker;
use xibalba\ocelote\ArrayHelper;

use xibalba\alpaca\exceptions\Alpaca as AlpacaException;

/**
 * Abstracts the Create, Update and Delete operations on static methods.
 * Alo provide fetch{} methods for common select operations.
 *
 * @package xibalba\alpaca
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Keeper {
	use DbAware;

	const FETCH_MODEL = 0x01;
	const FETCH_ARRAY = 0x02;

	/**
	 * @param Model $model
	 * @return array id condition of passed Model.
	 */
	protected static function getIdCondition(Model $model) {
		$conditions = null;
		$idProperty = $model->getIdProperty();

		if(is_array($idProperty)) {
			$idCondition = [];

			foreach($idProperty as $idField) $idCondition[$model::getColumnName($idField, false, false)] = $model->getValue($idField);
			$conditions['AND'] = $idCondition;
		}
		else $conditions[$idProperty] = $model->getId();

		return $conditions;
	}

	protected static function handleDbError(array $error) {
		if(!empty($error) && $error[0] !== '00000') throw new DbException($error);
	}

	/**
	 * Return a loaded Model instance of passed Model class.
	 *
	 * @param string $modelClass Model class name to create objects.
	 * @param array $data Data to set on Model instance.
	 *
	 * @return Model instance of loaded model
	 */
	protected static function populate(string $modelClass, array $data) {
		if(Checker::isEmpty($data)) throw new AlpacaException('«$data» argument cannot be empty');
		$model = new $modelClass($data);
		$model->setLoaded();
		return $model;
	}

	/**
	 * Delete from database the data of passed model.
	 *
	 * @param Model $model
	 * @return bool
	 * @throws DbException
	 */
	public static function delete(Model $model) {
		$db = static::getDbConnection();

		$conditions = self::getIdCondition($model);
		$db->delete($model::getTableName(), $conditions);

		$error = $db->error();
		if(!empty($error) && $error[0] !== '00000') throw new DbException($error);

		return true;
	}

	/**
	 * Delete data from database by the passed $conditions
	 *
	 * @param string $modelClass Model class name.
	 * @param array | string $conditions Conditions to apply to DELETE.
	 *
	 * @return bool TRUE on success.
	 */
	public static function deleteBy(string $modelClass, $conditions = null) {
		$db = static::getDbConnection();
		$db->delete($modelClass::getTableName(), $conditions);
		static::handleDbError($db->error());
		return true;
	}

	/**
	 * Delete from database the data by the passed id match.
	 *
	 * @param string $modelClass
	 * @param mixed $id
	 *
	 * @return bool TRUE on success.
	 * @throws DbException
	 */
	public static function deleteById(string $modelClass, $id) {
		$db = static::getDbConnection();
		$idProperty = $modelClass::getIdProperty();
		$conditions = [];

		if(is_array($idProperty)) $conditions['AND'] = $id;
		else $conditions[$modelClass::getColumnName($idProperty, false, false)] = $id;

		$db->delete($modelClass::getTableName(), $conditions);

		$error = $db->error();
		if(!empty($error) && $error[0] !== '00000') throw new DbException($error);

		return true;
	}

	/**
	 * Find data by conditions for the $modelClass argument.
	 * If data is not found, false is returned when $fetch = FETCH_MODEL; otherwise,
	 * and empty array is returned when $fetch = FETCH_ARRAY.
	 * If data founded and $fetch = FETCH_MODEL then an array of $modelClass instances
     * is returned, otherwise an array of array with data is returned.
	 *
	 * @param string $modelClass Class name of the model. Use ModelClass::class for get it.
	 * @param array $fields. '*' or null for get all fields of the model. Array with specific field names
	 * for get those fields data.
	 * @param array $conditions The prepared conditions for data search.
	 * @param integer $fetch Fetch mode. Use Finder constants for avoid errors. 'MODEL' and 'ARRAY' fetch modes
	 * are allowed.
	 *
	 * @return array Data founded.
	 */
	public static function fetchBy(string $modelClass, $fields = null, $conditions = null, int $fetch = self::FETCH_MODEL, bool $returnOneAsArray = true) {
		$db = static::getDbConnection();
		$fieldsToSelect = [];

		if($fields === null || $fields === '*') $fieldsToSelect = $modelClass::getColumnNames();
		else foreach($fields as $fieldName) $fieldsToSelect[] = $modelClass::getColumnName($fieldName);

		$data = $db->select($modelClass::getTableName(), $fieldsToSelect, $conditions);

		if($fetch == self::FETCH_ARRAY) {
			if(Checker::isIndexed($data)) return $data;
			else {
				if($returnOneAsArray) return [$data];
				else return $data;
			}
		}

		$r = [];
		if(Checker::isIndexed($data)) foreach($data as $row) $r[] = static::populate($modelClass, $row);
		else $r[] = static::populate($modelClass, $data);

		return $r;
	}

	/**
	 * @deprecated Please use fetchBy()
	 */
	public static function findBy($modelClass, $fields = null, $conditions = null, $fetch = self::FETCH_MODEL, $returnOneAsArray = true) {
		return static::fetchBy($modelClass, $fields, $conditions, $fetch);
	}

	/**
	 * Find all data for the $modelClass argument.
	 * If data is not found, then an empty array is returned.
	 *
	 * @param string $modelClass Class name of the model. Use ModelClass::class for get it.
	 * @param array $fields. '*' or null for get all fields of the model. Array with specific field names
	 * for get those fields data.
	 * @param integer $fetch Fetch mode. Use Finder constants for avoid errors. 'MODEL' and 'ARRAY' fetch modes
	 * are allowed.
	 *
	 * @return array Data founded.
	 */
	public static function fetchAll(string $modelClass, $fields = null, int $fetch = self::FETCH_MODEL) {
		return static::fetchBy($modelClass, $fields, null, $fetch);
	}

	/**
	 * @deprecated Plese use fetchAll()
	 */
	public static function findAll($modelClass, $fields = null, $fetch = self::FETCH_MODEL) {
		return static::fetchAll($modelClass, $fields, null, $fetch);
	}

	/**
	 * Find specific data from a model by id.
	 * The return can be an array or a $modelClass instance.
	 *
	 * @param string $modelClass Class name of the model. Use ModelClass::class for get it.
	 * @param mixed $id Id value. Scalar for a single field, array with key value par formated as «fied_name => value»
	 * for multiple fields.
	 * @param array $fields . '*' or null for get all fields of the model. Array with specific field names
	 * for get those fields data.
	 * @param integer $fetch Fetch mode. Use Finder constants for avoid errors. 'MODEL' and 'ARRAY' fetch modes
	 * are allowed.
	 * @return mixed The founded data. If the user want fetch as Model, and data has no found, then
	 * return false. If the user want fetch as array, and data has no found, then an empty array is
	 * returned.
	 *
	 * @throws AlpacaException
	 */
	public static function fetchById(string $modelClass, $id, $fields = null, int $fetch = self::FETCH_MODEL) {
		$db = static::getDbConnection();
		$idProperty = $modelClass::getIdProperty();
		$fieldsToSelect = [];
		$conditions = ['LIMIT' => 1];

		if(is_string($idProperty)) $conditions[$modelClass::getColumnName($idProperty, '', false)] = $id;
		else if(is_array($idProperty)) {
			$pk = [];
			foreach($id as $key => $val) $pk[$modelClass::getColumnName($key, '', false)] = $val;
			$conditions['AND'] = $pk;
		}
		else throw new AlpacaException("$modelClass has no an ".'$idProperty defined.');

		if($fields === null || $fields === '*') $fieldsToSelect = $modelClass::getColumnNames();
		else foreach($fields as $fieldName) $fieldsToSelect[] = $modelClass::getColumnName($fieldName);

		$data = $db->selectScalar($modelClass::getTableName(), $fieldsToSelect, $conditions);

		if($fetch == self::FETCH_ARRAY) return $data;
		else if(!Checker::isEmpty($data)) return static::populate($modelClass, $data);

		return false;
	}

	/**
	 * @deprecated Please use fetchById()
	 */
	public static function findById($modelClass, $id, $fields = null, $fetch = self::FETCH_MODEL) {
		return static::fetchById($modelClass, $id, $fields, $fetch);
	}

	/**
	 * Find specific data from a model by an field setted as unique.
	 * The return can be an array or a $modelClass instance.
	 *
	 * @param string $modelClass Class name of the model. Use ModelClass::class for get it.
	 * @param array $uniqueCondition A key => value pair that match with the unique field and value for search.
	 * @param array $fields . '*' or null for get all fields of the model. Array with specific field names
	 * for get those fields data.
	 * @param integer $fetch Fetch mode. Use Finder constants for avoid errors. 'MODEL' and 'ARRAY' fetch modes
	 * are allowed.
	 * @return mixed The founded data. If the user want fetch as Model, and data has no found, then
	 * return false. If the user want fetch as array, and data has no found, then an empty array is
	 * returned.
	 *
	 * @throws AlpacaException
	 */
	public static function fetchByUnique(string $modelClass, $uniqueCondition, $fields = null, int $fetch = self::FETCH_MODEL) {
		$db = static::getDbConnection();
		$fieldsToSelect = [];

		if($fields === null || $fields === '*') $fieldsToSelect = $modelClass::getColumnNames();
		else foreach($fields as $fieldName) $fieldsToSelect[] = $modelClass::getColumnName($fieldName);

		$key = array_keys($uniqueCondition)[0];

		if(!$modelClass::hasField($key)) throw new AlpacaException("« $key » does not exist as filed of $modelClass.");
		$condition[$modelClass::getColumnName($key, '', false)] = $uniqueCondition[$key];

		$data = $db->selectScalar($modelClass::getTableName(), $fieldsToSelect, $condition);
		if($fetch == self::FETCH_ARRAY) return $data;

		if(!Checker::isEmpty($data)) return static::populate($modelClass, $data);
		throw new AlpacaException('No data has be found for unique param.');
	}

	/**
	 * @deprecated Please use fetchByUnique()
	 */
	public static function findByUnique($modelClass, $uniqueCondition, $fields = null, $fetch = self::FETCH_MODEL) {
		return static::fetchByUnique($modelClass, $uniqueCondition, $fields, $fetch);
	}

	/**
	 * Find the fistr matched data for the passed condition.
	 * The return can be an array or a $modelClass instance.
	 *
	 * @param string $modelClass Class name of the model. Use ModelClass::class for get it.
	 * @param array $condition Array of conditions for search.
	 * @param array $fields. '*' or null for get all fields of the model. Array with specific field names
	 * for get those fields data.
	 * @param integer $fetch Fetch mode. Use Finder constants for avoid errors. 'MODEL' and 'ARRAY' fetch modes
	 * are allowed.
	 *
	 * @return mixed The founded data. If the user want fetch as Model, and data has no found, then
	 * return false. If the user want fetch as array, and data has no found, then an empty array is
	 * returned.
	 */
	public static function fetchFirstBy(string $modelClass, $conditions, $fields = null, int $fetch = self::FETCH_MODEL) {
		$db = static::getDbConnection();
		$fieldsToSelect = [];

		if($fields === null || $fields === '*') $fieldsToSelect = $modelClass::getColumnNames();
		else foreach($fields as $fieldName) $fieldsToSelect[] = $modelClass::getColumnName($fieldName);

		$data = $db->selectScalar($modelClass::getTableName(), $fieldsToSelect, $conditions);

		if($fetch == self::FETCH_ARRAY) return $data;
		if($data === false) return false;

		return static::populate($modelClass, $data);
	}

	/**
	 * @deprecated Please use fetchFirstBy()
	 */
	public static function findFirstBy($modelClass, $conditions, $fields = null, $fetch = self::FETCH_MODEL) {
		return static::fetchFirstBy($modelClass, $conditions, $fields, $fetch);
	}

	/**
	 * Find a single scalar field fram a model.
	 *
	 * @param string $modelClass Model class from get data.
	 * @param mixed $id Id (PK) of model
	 * @param string $fieldName Field name to load.
	 * @return mixed Founded data. false if data s not found.
	 *
	 * @throws AlpacaException
	 * @throws \Exception
	 */
	public static function fetchScalarById(string $modelClass, $id, string $fieldName) {
		if(!$modelClass::hasField($fieldName)) throw new \Exception("«$fieldName» must be a valid field name of «­$modelClass» model.");
		$db = static::getDbConnection();

		$idProperty = $modelClass::getIdProperty();
		$columnName = $modelClass::getColumnName($fieldName);

		$conditions = [];

		if(is_array($idProperty)) {
			$tmp = [];

			foreach ($id as $key => $val) {
				$tmp[$modelClass::getColumnName($key, false, false)] = $val;
			}

			$conditions['AND'] = $tmp;
		}
		else $conditions[$modelClass::getColumnName($idProperty, false, false)] = $id;

		$data = $db->selectScalar($modelClass::getTableName(), $columnName, $conditions);
		return $data;
	}

	public static function fetchScalarByUnique(string $modelClass, $uniqueCondition, string $fieldName) {
		if(!$modelClass::hasField($fieldName)) throw new \Exception("«$fieldName» must be a valid field name of «­$modelClass» model.");

		$db = static::getDbConnection();
		$condition = ['LIMIT' => 1];
		$columnName = $modelClass::getColumnName($fieldName);

		$key = array_keys($uniqueCondition)[0];

		if(!$modelClass::hasField($key)) throw new AlpacaException("« $key » does not exist as filed of $modelClass.");
		$condition[$modelClass::getColumnName($key, '', false)] = $uniqueCondition[$key];

		$data = $db->select($modelClass::getTableName(), $columnName, $condition);
		return $data;
	}

	/**
	 * @deprecated Please use fetchScalarById()
	 */
	public static function findScalarById($modelClass, $id, $fieldName = '') {
		return static::fetchScalarById($modelClass, $id, $fieldName);
	}

	protected static function generateId(string $modelClass) {
		$db = static::getDbConnection();

		$idProperty = $modelClass::getIdProperty();
		if(is_string($idProperty)){
			$maxId = $db->max($modelClass::getTableName(), $modelClass::getColumnName($modelClass::getIdProperty(), '', false));
			$maxId++;
			return $maxId;
		}
		else throw new AlpacaException('«KEEPER_AUTO_STRATEGY» allowed only for sigle field id.');
	}

	/**
	 * @param Model $model
	 * @throws AlpacaException
	 * @throws DbException
	 * @throws \Exception
	 */
	public static function persist(Model $model) {
		if($model->isDirty()) {
			$db = static::getDbConnection();

			if($model->isNew()) {
				$s = $model::getStrategy();

				if($s !== $model::USER_MANUAL_STRATEGY) {
					if($s !== $model::DB_AUTO_STRATEGY) {
						if($s === $model::KEEPER_AUTO_STRATEGY) $model->setId(static::generateId($model));
						else if ($s === $model::PHP_UUID_STRATEGY) $model->setId(uniqid());
						else if($s === $model::MODEL_MANUAL_STRATEGY) {
							if(method_exists($model, 'generateId')) $model->setId($model::generateId());
							else throw new AlpacaException('Model require a generateId() user defined public method.');
						}
						else throw new AlpacaException('Strategy not supported.');
					}
				}

				$values = self::prepareFields($model);

				if($s === $model::DB_AUTO_STRATEGY) {
					$db->insert($model::getTableName(), $values);
					$model->setId($db->getLastInsertId());
				}
				else $db->insert($model::getTableName(), $values);
			}
			else {
				$conditions = self::getIdCondition($model);

				$values = self::prepareFields($model, true);
				$db->update($model::getTableName(), $values, $conditions);
			}

			static::handleDbError($db->error());
			$model->setLoaded();
		}
	}

	/**
	 * Take an array of fields - values pair and intersect the key fields
	 * string with the column name for that field and return the result array.
	 *
	 * If the model has a som fields like:
	 *
	 *   ...
	 *   'field_a' => ['type' => 'string', 'column_name' => 'column_name_y'],
	 *   'field_b' => ['type' => 'string', 'column_name' => 'column_name_z'],
	 *   ...
	 *
	 * An array for conditions:
	 *
	 *   $myFields = ['field_a' => 'abc val', 'field_b' => 'xyz val'];
	 *   $intersected = Keeper::fieldsIntersecColumns(SomeModel::class, $myFields);
	 *
	 * Thes `$intersected` will be like:
	 *
	 *   ['column_name_z' => 'abc val', 'column_name_z' => 'xyz val']
	 *
	 * This mothed is useful for prepare custom conditions that are non contempled by
	 * the default Keeper API.
	 *
	 * @param sring $modelClass Model class name tu use.
	 * @param array $fields The array with fields - values pairs.
	 *
	 * @return array The resul array
	 */
	public static function fieldsIntersecColumns(string $modelClass, array $fields) : array {
		$r = [];
		foreach ($fields as $key => $val) $r[$modelClass::getColumnName($key, '', false)] = $val;
		return $r;
	}

	public static function prepareFields(Model $model, bool $modifiedOnly = false) : array {
		$values = [];
		$fields = [];

		if($modifiedOnly) $fields = $model->getModifiedFieldsNames();
		else $fields = $model::getFieldsNames();

		if($model::getStrategy() == $model::DB_AUTO_STRATEGY) ArrayHelper::searchAndRemove($fields, $model::getIdProperty());

		foreach($fields as $fieldName) {
			$fieldsDefinitions = $model::getFieldsDefinitions($fieldName);
			$columnName = (isset($fieldsDefinitions['column_name'])) ? $fieldsDefinitions['column_name'] : $fieldName;
			//Para las fechas debe usarse «db_date_format»
			if(
				is_array($fieldsDefinitions)
				&& isset($fieldsDefinitions['type'])
				&& ($fieldsDefinitions['type'] == 'date' || $fieldsDefinitions['type'] == 'datetime')
			) {
				$date = $model->getRawValue($fieldName);

				if($date instanceof \DateTime) {
					$dateFromat = ($fieldsDefinitions['type'] == 'date') ? $model::DEFAULT_DB_DATE_FORMAT : $model::DEFAULT_DB_DATETIME_FORMAT;

					if(isset($fieldsDefinitions['db_date_format'])) $dateFromat = $fieldsDefinitions['db_date_format'];
					$values[$columnName] = $date->format($dateFromat);
				}
				else $values[$columnName] = $date;
			}
			else {
				$tmpVal = $model->getValue($fieldName);
				if($tmpVal !== null) $values[$columnName] = $tmpVal;
			}
		}

		return $values;
	}

	/**
	 * @todo Add description
	 *
	 * @param string $modelClass
	 * @param array $data
	 * @param array $conditions
	 * @return bool
	 */
	public static function updateBy(string $modelClass, array $data, array $conditions) : bool {
		$db = static::getDbConnection();

		$data = static::fieldsIntersecColumns($modelClass, $data);
		$conditions = static::fieldsIntersecColumns($modelClass, $conditions);

		if(count($conditions) > 1) $conditions = ['AND' => $conditions];

		$db->update($modelClass::getTableName(), $data, $conditions);

		static::handleDbError($db->error());
		return true;
	}

	/**
	 * @todo Add description and examples
	 *
	 * @param string $modelClass
	 * @param $id
	 * @param array $data
	 * @return bool
	 * @throws DbException
	 */
	public static function updateById(string $modelClass, $id, array $data) {
		$db = static::getDbConnection();
		$idProperty = $modelClass::getIdProperty();
		$conditions = [];

		if(is_array($idProperty)) {
			$idData = [];
			foreach($id as $key => $val) {
				$colName = $modelClass::getColumnName($key, false, false);
				$idData[$colName] = $val;
			}

			$conditions['AND'] = $idData;
		}
		else {
			$idProperty = $modelClass::getColumnName($modelClass::getIdProperty(), false, false);
			$conditions[$idProperty] = $id;
		}

		$data = static::fieldsIntersecColumns($modelClass, $data);

		$db->update($modelClass::getTableName(), $data, $conditions);

		$error = $db->error();
		if(!empty($error) && $error[0] !== '00000') throw new DbException($error);

		return true;
	}
}