<?php
/**
 * @copyright	2014 - 2017 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba//alpaca
 */

namespace xibalba\alpaca;

use xibalba\alpaca\exceptions\Alpaca as AlpacaException;

use xibalba\ocelote\Inflector;
use xibalba\ocelote\StringHelper;
use xibalba\ocelote\ArrayHelper;

/**
 * A Model is a map of a database table structure.
 * Provides an abstraction for a painless data manipulation.
 *
 * Each table is compose by *fields* with its restrictions,
 * so each Model must maps all those elements.
 *
 * You must create your own Model classes extending this class:
 *
 * ```php
 * <?php
 *
 * class YourModel extends xibalba\alpaca\Model{
 *     protected $_fields = [];
 *     protected $_idProperty = 'id';
 *
 *     public static function getTableName(){
 *         return 'table_model'
 *     }
 * }
 * ```
 *
 * «idProperty» is optional, 'id' by default.
 * Override «getTableName()» methods only if your class name does not match with the
 * table name formated as snake_case and pluralized.
 *
 * @package xibalba\alpaca
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
abstract class Model {

	const DB_AUTO_STRATEGY		= 0x00;
	const KEEPER_AUTO_STRATEGY	= 0x01;
	const MODEL_MANUAL_STRATEGY = 0x02;
	const USER_MANUAL_STRATEGY	= 0x03;
	const PHP_UUID_STRATEGY 	= 0x04;

	const DEFAULT_DB_DATE_FORMAT	 = 'Y-m-d';
	const DEFAULT_DB_DATETIME_FORMAT = 'Y-m-d G:i:s';
	const DEFAULT_DATE_FORMAT		 = 'd/m/Y';
	const DEFAULT_DATETIME_FORMAT	 = 'd/m/Y';

	/**
	 * @const Data types that can be used by a «field»,
	 * this types are the same scalar types of PHP,
	 * except for «date» and «datetime», this types are
	 * convert to «\DateTime» objects.
	 */
	protected static $_vTypes = [
		'integer',
		'int',
		'float',
		'string',
		'boolean',
		'bool',
		'date',
		'datetime'
	];

	/**
	 * @var string|array $_idProperty define the property for indentify the model as unique.
	 * This property is analog to the table PK.
	 * If the id is compose by one field, then must match with a field name defined on $_fields property.
	 * If the id is compose by two or more fields, them must be an array with the fields names.
	 *
	 * If a model does not require an id, then must be set like «null». In this case, you
	 * can't use «getById()» methods.
	 *
	 * «id» string by default
	 */
	protected static $_idProperty = 'id';

	/**
	 * Must have the form:
	 * [
	 *     'fieldName' => [
	 *         'type' => *type*,
	 *         {'column_name' => 'table_column_name',}
	 *         {'default_value' => 'default value'}
	 *     ]
	 * ]
	 * The elementes between brackets ({}) are optional.
	 * Ever must define a type.
	 *
	 * For valid types,
	 * @see $_vTypes property
	 *
	 * @var array $fields Maps the model fields, with column names (if apply),
	 * restrictions and types.
	 */
	protected static $_fields = [];

	protected static $_strategy = self::DB_AUTO_STRATEGY;

	/**
	 * @var boolean $_isNew define if a model represents a new record or not.
	 * true by default.
	 */
	protected $_isNew = true;

	/**
	 * @var array $_data Store data model.
	 */
	protected $_data = [];

	/**
	 * @var $_modified Store the last changed data.
	 */
	protected $_modified = [];

	/**
	 * @var boolean $_isDirty define if a model id dirty and must be updated
	 * by the Store who manage it.
	 */
	protected $_isDirty = false;

	/**
	 * If are defined default data, this are loaded on construction time.
	 *
	 * @param array $initialData Optional initial data
	 */
	function __construct(array $initialData = []) {
		$this->buildFields();
		if(!empty($initialData)) $this->setValues($initialData);
	}

	/**
	 * Build the internal data array setting default values when apply.
	 */
	protected function buildFields() {
		foreach(static::getFields() as $fieldName => $definitions) {
			$this->_data[$fieldName] = $definitions['default_value'] ?? null;
		}
	}

	/**
	 * Retrive the fields array.
	 *
	 * An array of filed names can be passed for return only those fields.
	 *
	 * @return array Model fields.
	 */
	protected static function getFields(array $fields = []) : array {
		if(empty($fields)) return static::$_fields;

		$fieldNames = array_keys(static::$_fields);

		$validation = array_values(array_intersect($fieldNames, $fields));

		if($validation !== $fields) {
			$diff = array_diff($validation, $fieldNames);
			$msg = 'Invalid requested fields:  ' . implode(', ', $diff);
			throw new AlpacaException($msg);
		}

		$result = [];
		foreach ($fields as $fieldKey) $result[$fieldKey] = static::$_fields[$fieldKey];

		return $result;
	}

	/**
	 * Return a column names array.
	 * A prefix can be passed. By default the table name is the prefix.
	 *
	 * If is defined «column_name» the return string have the form «column_name AS field_name»
	 *
	 * @param string $prefix Optional prefix
	 * @return array Columns names.
	 */
	public static function getColumnNames(string $prefix = '', bool $returnAsPart = true, array $fields = []) : array {
		if(empty($prefix)) $prefix = static::getTableName();

		if(empty($fields)) $fields = static::getFields();
		else $fields = static::getFields($fields);

		$c = [];
		foreach($fields as $fieldName => $definitions) {
			$c[] = static::getColumnName($fieldName, $prefix, $returnAsPart);
		}

		return $c;
	}

	/**
	 * @param string $fieldName Configured field name
	 * @param string | boolean $prefix Optional prefix. Table name by default.
	 * «false» for does not use prefix.
	 * @param boolean $returnAsPart False does not return AS «column_name» part, usefull for
	 * build personalized conditions. Just have sens when are defined «column_name» element.
	 *
	 * @return bool|string false if no name if found. The formated string if column is found.
	 */
	public static function getColumnName(string $fieldName, $prefix = '', bool $returnAsPart = true) {
		if(is_string($prefix) && empty($prefix)) $prefix = static::getTableName();

		if(array_key_exists($fieldName, static::getFields())) {
			$definitions = static::getFields()[$fieldName];
			if(is_array($definitions)) {
				if(isset($definitions['column_name'])) {
					$c = '';

					if($prefix === false) $c = $definitions['column_name'];
					else $c = $prefix.'.'.$definitions['column_name'];

					if($returnAsPart) $c .= '('.$fieldName.')';
					return $c;
				}
				else {
					if($prefix === false) return $fieldName;
					return $prefix.'.'.$fieldName;
				}
			}
			if($prefix === false) return $fieldName;
			return $prefix.'.'.$fieldName;
		}
		return false;
	}

	/**
	 * Return an array with fields names defined for the model.
	 * @return array fieldsNames Fields names.
	 */
	public static function getFieldsNames() : array {
		return array_keys(static::getFields());
	}

	/**
	 * Return an array with fields names that has been modified.
	 * @return array fields names.
	 */
	public function getModifiedFieldsNames() : array {
		return array_keys($this->_modified);
	}

	/**
	 * Checks if a filed name exist on $_fields property.
	 *
	 * @param string $fieldName Name field to check.
	 * @return boolean whenever is setted.
	 */
	public static function hasField(string $fieldName) : bool {
		return array_key_exists($fieldName, static::getFields());
	}

	/**
	 * Try to return the value definition for the specified field.
	 * If the passed fieldname does not exist the an Exception is rised.
	 *
	 * @param string $fieldName
	 * @param string $def
	 *
	 * @return mixed Definition data.
	 * @throws \Exception
	 */
	public static function getFieldDefinition(string $fieldName, string $def) {
		$fields = static::getFields();
		if(array_key_exists($fieldName, $fields)) {
			$definitions = $fields[$fieldName];
			if(is_array($definitions)){
				if(isset($definitions[$def])) return $definitions[$def];
				else throw new \Exception('No «'.$def.'» found on «'.$fieldName.'» option.');
			}
			else throw new \Exception('No definitions found for «'.$fieldName.'».');
		}

		throw new \Exception('«$_fields» property has no «'.$fieldName.'» defined.');
	}

	/**
	 * Check if an specific definition name exist on a field.
	 *
	 * @param string $fieldName
	 * @param string $def
	 *
	 * @return bool TRUE if definition existe, FALSE otherwise.
	 * @throws \Exception
	 */
	public static function hasFieldDefinition(string $fieldName, string $def) : bool {
		$fields = static::getFields();
		if(array_key_exists($fieldName, $fields)) {
			$definitions = $fields[$fieldName];
			if(is_array($definitions)) {
				if(isset($definitions[$def])) return true;
				else return false;
			}
			else throw new \Exception('No definitions found for «'.$fieldName.'».');
		}

		throw new \Exception('«$_fields» property has no «'.$fieldName.'» defined.');
	}

	/**
	 * Return the definitions for the specified field.
	 * @param string $fieldName Definitions
	 *
	 * @return mixed All definitions for passed fieldname.
	 * @throws \Exception
	 */
	public static function getFieldsDefinitions(string $fieldName) {
		$fields = static::getFields();
		if(array_key_exists($fieldName, $fields)) {
			$definitions = $fields[$fieldName];
			if(is_array($definitions)) return $definitions;
			return null;
		}

		throw new \Exception('«$_fields» property has no «'.$fieldName.'» defined.');
	}

	/**
	 * @return string configured id property (also can be null).
	 */
	public static function getIdProperty() {
		return static::$_idProperty;
	}

	/**
	 * @return string table name
	 */
	public static function getTableName() : string {
		return Inflector::tableize(StringHelper::basename(static::class));
	}

	/**
	 * @return integer PK generation strategy
	 */
	public static function getStrategy() : int {
		return static::$_strategy;
	}

	/**
	 * @return boolean True for a dirty model, otherwise false.
	 */
	public function isDirty() : bool {
		return $this->_isDirty;
	}

	/**
	 * A new model is when has no id.
	 * @return boolean True for a new model, otherwise false.
	 */
	public function isNew() : bool {
		return $this->_isNew;
	}

	/**
	 * Return the id data.
	 * A scalar value is returned if the id is compose by one field,
	 * otherwise is returned an array.
	 * @return mixed id data.
	 */
	public function getId() {
		$idProperty = self::getIdProperty();
		if($idProperty === null) return null;
		else {
			if(is_array($idProperty)) {
				$id = [];
				foreach($idProperty as $idEl) $id[] = $this->getValue($idEl);

				return $id;
			}
			else return $this->getValue($idProperty);
		}
	}

	/**
	 * Return the value for the specified field.
	 *
	 * @param string $fieldName Field name to get data.
	 *
	 * @return mixed the stored data value.
	 * @throws \Exception
	 */
	public function getValue(string $fieldName) {
		$fields = static::getFields();
		if(array_key_exists($fieldName, $fields)) {
			$definitions = $fields[$fieldName];
			if(
				is_array($definitions)
				&& isset($definitions['type'])
				&& ($definitions['type'] == 'date' || $definitions['type'] == 'datetime')
			) {
				$dateVal = $this->_data[$fieldName];
				if($dateVal instanceof \DateTime) {
					if(isset($definitions['date_format'])) $dateFromat = $definitions['date_format'];
					else $dateFromat = ($definitions['type'] == 'date') ? static::DEFAULT_DATE_FORMAT : static::DEFAULT_DATETIME_FORMAT;
					return $dateVal->format($dateFromat);
				}
				return null;
			}
			return $this->_data[$fieldName];
		}
		throw new \Exception('«$_fields» property has no «'.$fieldName.'» defined.');
	}

	/**
	 * Return all data values as array.
	 *
	 * @param array $fieldsNames Fields names to return data values.
	 * If is an empty array, then all data values will returned,
	 * Even those no loaded (null will be returned for those fields).
	 * If is an array, then will be returned data values for those fields than match
	 * with the fields definedn on «$_fields» property.
	 * «null» will be returned for non defined fields
	 * @param $returnNulls set it to false for no return undefined fields.
	 *
	 * @return array Key value pair array with the stored data values.
	 */
	public function getValues(array $fieldsNames = [], bool $returnNulls = true) : array {
		$values = [];

		if(empty($fieldsNames) || $fieldsNames == '*') $fieldsNames = array_keys(static::getFields());

		foreach($fieldsNames as $fieldName) {
			if(isset($this->_data[$fieldName])) $values[$fieldName] = $this->getValue($fieldName);
			else if($returnNulls) $values[$fieldName] = null;
		}

		return $values;
	}

	/**
	 * Return raw value for the specified field.
	 * For example, for a date type field, the \DateTime instance will
	 * be returned instead the formated value.
	 *
	 * This methos is faster that «getValues()».
	 *
	 * @param string $fieldName Field name to get data value.
	 * @throws \Exception
	 */
	public function getRawValue(string $fieldName) {
		if(array_key_exists($fieldName, static::getFields())) return $this->_data[$fieldName];
		throw new \Exception('«$_fields» property has no «'.$fieldName.'» defined.');
	}

	/**
	 * Return an array with the raw values.
	 *
	 * @param array $fieldsNames
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function getRawValues(array $fieldsNames = []) : array {
		$values = [];

		if(!empty($fieldsNames)) {
			foreach($fieldsNames as $fieldName) {
				$values[$fieldName] = isset($this->_data[$fieldName]) ? $this->getRawValue($fieldName) : null;
			}
		}
		else {
			$fieldsNames = array_keys(static::getFields());
			foreach($fieldsNames as $fieldName) {
				$values[$fieldName] = $this->getRawValue($fieldName);
			}
		}
		return $values;
	}

	public function setClean() {
		$this->_isDirty = false;
	}

	public function setDirty() {
		$this->_isDirty = true;
	}

	/**
	 * Set the id (PK) of the model.
	 * If id is a single field, a scalar value is expected.
	 * If id is composed by many fields, an arrar is expected.
	 *
	 * @param mixed $id model id.
	 * @throws \Exception
	 */
	public function setId($id) {
		$idProperty = self::getIdProperty();
		if(is_array($idProperty)) {
			if(!is_array($id)) throw new \Exception('«id» must be an array.');

			foreach($idProperty as $fieldName) {
				$this->setValue($fieldName, $id[$fieldName]);
			}
		}
		else $this->setValue($idProperty, $id);
	}

	/**
	 * Set the model as loaded. This means that model data
	 * and db data are synchronized.
	 */
	public function setLoaded() {
		$this->_isNew = false;
		$this->_modified = [];
		$this->setClean();
	}

	/**
	 * Set a value for the specified field.
	 *
	 * @param string $fieldName Field name to set data.
	 * @param mixed $value Data value to set.
	 *
	 * @throws \Exception
	 */
	public function setValue(string $fieldName, $value) {
		if(array_key_exists($fieldName, $this->_data)) {
			/**
			 * Ensure data type.
			 * If the data type is «date» or «datetime», then
			 * create a \DateTime instance.
			 */
			$fiedlDefinitions = static::getFields()[$fieldName];
			if(is_array($fiedlDefinitions)) {
				if(isset($fiedlDefinitions['type'])) {
					$type = $fiedlDefinitions['type'];

					if(in_array($type, self::$_vTypes)) {
						if($type !== 'date' && $type !== 'datetime') {
							if(isset($fiedlDefinitions['allow_null'])) {
								if($fiedlDefinitions['allow_null'] === false) settype($value, $type);
							}
							else settype($value, $type);
						} else {
							if(!($value instanceof \DateTime)) {
								$dwFromat = ($type == 'date') ? static::DEFAULT_DB_DATE_FORMAT : static::DEFAULT_DB_DATETIME_FORMAT;
								if(isset($fiedlDefinitions['db_date_format'])) $dwFromat = $fiedlDefinitions['db_date_format'];

								$value = \DateTime::createFromFormat($dwFromat, $value);
							}
						}
					}
					else {
						throw new \Exception(
							get_called_class().' «'.$fieldName.'» has an invalid type.'.
							"\nDefined type: \"".$type.'"'.
							"\nAllowed types:".json_encode(self::$_vTypes)
						);
					}
				}
			}

			if($this->_data[$fieldName] !== $value) {
				$this->_modified[$fieldName] = $this->_data[$fieldName];
				$this->_data[$fieldName] = $value;
				$this->setDirty();
			}
		}
	}

	/**
	 * Set many values from an array.
	 * param array $vaues values to set. Must be formated as «['fieldName' => data]»
	 *
	 * @param array $values
	 * @throws \CException
	 */
	public function setValues(array $values = []) {
		if(!is_array($values)) throw new \Exception(get_called_class() . ' $values must be an array.');

		foreach($values as $fieldName => $val) {
			$this->setValue($fieldName, $val);
		}
	}
}
