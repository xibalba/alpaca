<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/alpaca
 */

namespace xibalba\alpaca;

use xibalba\tuza\DbConnection as BaseAbstract;
use xibalba\tuza\DbException;

/**
 * Class DbConnection provide a Tuza DbConnection extension over PDO
 * for manage db connection.
 *
 * @package xibalba\alpaca
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class DbConnection extends BaseAbstract {
	/**
	 * Execute a sql query and return the result.
	 *
	 * @throws DbException
	 */
	public function query(string $query) {
		$res = parent::query($query);

		$error = $this->getError();
		if(!empty($error) && $error[0] !== '00000') throw new DbException($error);

		return $res;
	}
}
