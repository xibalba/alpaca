<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba//alpaca
 */

namespace xibalba\alpaca\utility;

use xibalba\alpaca\Model;
use xibalba\alpaca\utility\traits\FieldsMergeable;

/**
 * Use this class when you need add «label - description» fields
 * to your models.
 *
 * @package xibalba\alpaca\utility
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
abstract class Descriptible extends Model {
	use FieldsMergeable;
	
	static protected $_fields = [
		'id' 	  	  => ['type' => 'integer'],
		'label' 	  => ['type' => 'string'],
		'description' => ['type' => 'string']
	];
}