<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba//alpaca
 */

namespace xibalba\alpaca\utility\traits;

use xibalba\ocelote\ArrayHelper;

/**
 * This trait allow to a Model merge its fields with parent fields.
 *
 * @package xibalba\alpaca\utility\traits
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
trait FieldsMergeable {
	protected static $_mergedFields = false;

	protected static function getFields(array $fields = []) : array {
		if(static::$_mergedFields) return static::$_fields;
		else {
			$fields = ArrayHelper::merge(static::$_fields, parent::$_fields);
			static::$_mergedFields = true;
			return static::$_fields = $fields;
		}
	}
}
