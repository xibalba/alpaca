<?php
/**
 * @copyright	2014 - 2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba//alpaca
 */

namespace xibalba\alpaca\utility;

/**
 * Use this class when you want use parameterization on your project.
 * This class represent a param compose by:
 * 
 * * id
 * * label
 * * description
 * * observation
 * * domain
 *
 * @package xibalba\alpaca\utility
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */

class Param extends Descriptible {
	protected static $_fields = [
		'observation' => ['type' => 'string'],
		'domain' 	  => ['type' => 'string']
	];
}
