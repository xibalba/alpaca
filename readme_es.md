Alpaca
======

Alpaca es un ORM ligero basado en el patrón [DataMapper](http://en.wikipedia.org/wiki/Datamapper) que se sustenta sobre el framework de base de datos ligero  [Medoo](http://medoo.in/), desarrollado por [Yeshua Rodas](https://bitbucket.org/yybalam) y el equipo de [Xibalba Lab](https://bitbucket.org/xibalba) en la [Universidad Pedagógica Nacional "Francisco Morazán"](https://bitbucket.org/upnfm/)

¿ORM?
-----

Un [ORM](http://es.wikipedia.org/wiki/ORM) es un mapeador objeto-relacionar. Abstrae la complejidad inherente del SQL *reduciendo* la mezcla de paradigmas (POO, relacional), lo que a su vez repercute en un desarrollo y mantenimiento de software mucho más sencillo e intuitivo. También se gana en flexibilidad, en la reducción de errores y en la facilidad para encontrarlos y corregirlos.

¿Otro ORM?
----------

Hay dos patrones que se siguen en el diseño de los ORM:

* [Registro activo](http://en.wikipedia.org/wiki/Active_record_pattern) (Active Record)
* [Mapa de datos](http://en.wikipedia.org/wiki/Data_mapper_pattern) (Data Mapper)

La mayoría de frameworks PHP actuales incluyen su propio ORM, generalmente bajo el patrón de Registro Activo.

Por otra parte, los ORM independientes actuales más popupales son grandes, pesados y complejos.

El principal objetivo de la arquitectura es es ligero, por eso se apoya en una librería tan ligera como Medoo, esta pesa solo 14KB. :D