Alpaca ORM
==========

Alpaca es un [ORM](http://es.wikipedia.org/wiki/Mapeo_objeto-relacional) ligero con patrón [Data Mapper](http://en.wikipedia.org/wiki/Data_mapper_pattern).

El ORM es la herramienta adecuada para cubrir la «M» del desarrollo de aplicaciones bajo el patrón MVC.

Las tareas recurrentes de toda aplicación son las conocidas [CRUD](http://es.wikipedia.org/wiki/CRUD), por lo general siendo una petición equivalente al llamado de estas operaciones.

Alpaca está diseñado bajo tres premisas principales:

1. Un *Modelo* (Model) es una clase ligera que mapea una tabla, es decir, sus columnas, restricciones y relaciones.
2. Un *Almacenista* (Keeper) es una clase ligera (métodos estáticos) que abstrae las operaciones CRUD más comunes:
	* Crear registros nuevos.
	* Actualizar registros existentes por ID o por condiciones preparadas.
	* Obtener modelos por ID o por condiciones preparadas.
	* Devolver los datos como arreglos o instancias de Modelo.
3. Un *Almacén* (Store) es una clase cuyas instancias manipulan los cambios (creación, edición, eliminación) de colecciones de modelos.

Cada elemento (Modelo, Almacenista y Almacén) es independiente uno del otro.

Esto presenta una serie de ventajas para el desarrollo de software:

* Representación de tablas en forma de clases ligeras, que resulta más natural en el paradigma orientado a objetos.
* Consultas esotéricas. Los buscadores no saben que van a buscar hasta el tiempo de ejecución, lo que se indica mediante el nombre de los modelos. Los datos se devuelven en forma de arreglos o instancias de modelo, lo que facilita la manipulación de los datos al momento de programar.
* Se utilizan los Almacenistas y Almacenes solo cuando es necesario, lo que resulta en ejecuciones más livianas, y por ende, más eficientes.

Modelo (Model)
--------------

Un modelo es una clase ligera que únicamente describe las especificaciones de una tabla, es decir, sus columnas, restricciones, tipos y relaciones.
Una instancia de un modelo únicamente almacena los valores correspondientes a los campos definidos en la clase, y brinda información acerca del estado del modelo, como ser si es *nuevo* (es decir, no se ha almacenado en la base de datos), si se le han hecho cambios a los datos y no se han registrado, o cualquier otro estado que se requiera seguir durante el tiempo de vida del modelo.

Almacenista (Keeper)
-----------------

Un Almacenista es una clase con métodos estáticos (en ello radica su ligereza) cuyo único fin es abstraer las operaciones CRUD más comunes para manipular modelos.
Los datos devueltos deben ser valores escalares, arreglos de datos, instancias de modelo, arreglos de arreglos de datos, o arreglos de instancias de modelos.

Almacén (Store)
---------------

Un almacén es un gestor de modelos. Puede obtener datos vía Almacenistas, almacenar modelos y seguir el flujo de estos para las operaciones CRUD. Un almacén trabaja únicamente con un tipo de modelo, si un modelo tiene relaciones, entonces un almacén puede contener otros almacenes responsables de los modelos relacionados.

Un almacén puede hacer todo el trabajo de un Buscador, pero hay que tener presente que los almacenes son más pesados que los buscadores, por lo que el uso de almacenes para obtener datos no es recomendado.