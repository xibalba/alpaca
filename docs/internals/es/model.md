﻿Modelo (Model)
==============

Un modelo es un mapeo de la estructura de una tabla de una base de datos.
Provee una abstracción para una sencilla manipulación de los datos.
Cada tabla se compone de *campos* con sus restricciones y las relaciones que apliquen según el caso, así, cada modelo debe mapear todos esos elementos.

El objetivo del modelo es servir como una interfaz transparente entre un registro almacenado en la base de datos y una estructura de tipo objeto al momento de manipular datos con PHP.

La idea es abstraer lo más posible el esquema Entidad-Relación de las bases de datos tradicionales y presentarlo como un objeto coherente al momento del desarrollo de aplicaciones; así, en lugar de trabajar con los conectores y fuentes de datos, se trabaja directamente con objetos que son fáciles de entender y manipular.

La definición de una clase modelo facilita la comprensión de **que** representa un conjunto de datos, trabajando con sustantivos. De esta manera, por ejemplo, en lugar de pensar en una tabla que contiene un listado de *llamas* con datos como el *nombre*, *rebaño*, *color de pelo*, etc, se piensa en un **objeto** Llama que posee campos bien definidos con esos atributos.

Desde el punto de vista de la POO (OOP), tiene mucho más sentido representar la realidad (un sustantivo) mediante una clase. Un programador novel que ya comprenda lo básico del paradigma de la POO entendería fácilmente un código como el siguiente:

    :::php
    $someLlama = new Llama();
    $someLlama->setValues([
        'birth' => '2014/02/01',
        'name' => 'Qwara'
    ]);
    
También se facilita el manejo de tipos. Así, el programador está conciente de que trabaja con los tipos de datos estándar de PHP. Es mucho más fácil entender que un dato es numérico, de cadena o de fecha que de `"VARCHAR"` o `"TEXT"`.

Desde luego, para permitir que la interfaz funcione correctamente, a cada campo se le debe definir sus propiedades y límites.

El trabajo con bases de datos mediante este enfoque es mucho más intuitivo, escalable y fácil de llevar tanto a nivel de anális, diseño y desarrollo, como en la etapa de mantenimiento del software.

Nombre de clase y tabla
-----------------------

Cada clase y tabla debe tener un nombre propio y único. Es recomendable que este nombre coincida entre la clase y la tabla. Por convención se espera que el nombre de la clase cumpla con la forma *CamelCase* mientras que el de la tabla sea  'snake_case'.
El nombre de la tabla es algo común a todos sus registro, por lo que la propiedad de la clase es **estática** y **pública**.

Campos ($fields)
----------------

El mapeado de columnas se realiza en la propiedad estática ***$fields*** del modelo. Esta propiedad no debe ser accedida directamente, sino por medio de métodos que validen y manipulen su contenido, por lo que es **protegida**.
Esta propiedad debe ser utilizada por los Buscadores (Finders) y Almacenes (Stores) para manipular adecuadamente la comunicación entre lo definido en el modelo y la tabla existente en la base de datos.

La propiedad ***$fields*** debe definirse como un arreglo de la siguiente forma:

    :::php
    $fields = [
        'field_name' => [__definiciones__]
    ];

Donde `'field_name'` es una cadena que nombra al campo. Si no se establecen definiciones para el campo, esta cadena **debe** coincidir con el nombre de la columna de la tabla que mapea.

Por otra parte, «__definiciones__»: es un arreglo que *puede* incluir una o todas de las siguientes definiciones:

* `column_name`: Establece el nombre del campo como está definido en la tabla. Si esta opción no se define, se asume que el nombre de la columna del campo en la tabla coincide con el nombre del campo del modelo.
* `type`: Establece el tipo de dato. Se soportan los tipos de datos primitivos de PHP:
    * integer
    * float
    * boolean
    * string
    Se soporta también un tipo «`date`», pero este se trata diferente, si un campo se define con este tipo, entonces el dato almacenado es una instancia de la clase DateTime de PHP.  Adicionalmente pueden definirse dos propiedades mas:

    * `date_set_format`: Establece el formato cuando se establece un valor de fecha
    * `date_get_format`: Establece el formato cuando se obtiene un valor de fecha.

* `dbtype`: Establece el tipo de dato correspondiente a la tabla. Esta opción se establece con el fin de crear tablas a partir de los modelos a travez de algún Gestor. Se podrían utilizar tipos específicos para un SGBD, pero esto limitaría la portabilidad, por lo que se recomienda el uso de los tipos definidos por el estándar SQL.

* `dimension` Establece el tamaño máximo que espera el campo. Esto se hace con fines de validación, así se puede validad que los datos ingresados estén dentro de los parámetros aceptados por la base datos. Hay que notar que aunque el tamaño máximo para una variable de PHP se toma a partir del valor definido en la directiva `PHP_INT_MAX`, este no necesariamente coincide con el valor máximo esperado por una columna en la base de datos.

* `default_value`: Establece un valor por defecto para establecer en el campo. Cuando se crea un nuevo modelo, este valor se establece al momento de la construcción del objeto.

* `unique`: Establece si el campo es de carácter único o no. Se espera un valor booleano. Por defecto los campos **NO** son unique, excepto cuando un campo corresponde a una PK.

ID y PK
-------

La propiedad protegida ***$idProperty*** se usa para poder establecer, opcionalmente, un `id` que indique que campos componen la PK de la base de datos.
Se espera uno de dos valores:

1. Una cadena si la PK de la tabla se compone por un campo único. Por ejemplo, cuando la PK es indicada por un entero autoincremental.

2. Un arreglo si la PK se compone de varios campos, es decir, cuando las PK son compuestas. Por ejemplo, para indicar el PK de un municipio que se compone de dos campos:

    :::php
    class CountryRegion extends xibalba\alpaca\Model{
        protected static $fields = [
            'country_id'   => ['type' => 'integer'],
            'id'           => ['type' => 'integer'],
            'oficial_name' => ['type' => 'string'],
            'description'  => ['type' => 'string']
        ];
        
        protected  $id = ['country_id', 'id']
    }

La propiedad ***$strategy*** se usa para establecer la estratégia de generación de PK. Se plantean tres escenarios:

1. `DB_AUTO_STRATEGY`: La base de datos genera automáticamente las PK, normalmente siendo enteros autoincrementales.
2. `ALPACA_AUTO_STRATEGY`: Se utiliza el método por defecto que debe poseer un Almacén (Store) para generar los PK.
3. 'MANUAL_STRATEGY': El usuario programa una estratégia personalizada para generar la PK. Si se establece esta estratégia pero no se sobreescribe el método del Almacén (Store) que gestione al modelo, entonces por defecto se utilizará una estratégia `ALPACA_AUTO_STRATEGY`.

Relacines
---------

A cada modelo se le puede definir una serie de relaciones con otros modelos.

El estado de los datos
----------------------

A las instancias de los modelos se les debe seguir diversos estados a lo largo de la vida del objeto, estos estados son:

* Si el modelo es nuevo, es decir, no tiene una PK asignada.
* Si el modelo está **sucio**, es decir, si se han realizado cambios en los valores de los datos y estos no se han sincronizado con la base de datos.
* Los datos anteriores del modelo. Por si se desea obtener los datos previos a una modificación.