Almacenista (Keeper)
====================

El Almacenista abstrae las operaciones CRUD más comunes.
Básicamente envuelve en métodos estáticos las sentencias `INSERT`, `UPDATE` Y `SELECT`.

El diseño ligero basado en métodos estáticos de esta clase se sustenta en las siguientes premisas:

* Las consultas más comunes identificadas en el desarrollo de software son:
    * Crear un registro nuevo al que se le genera su PK.
    * Crear un lote de registros a cada cual se le genera su PK.
    * Actualizar un registro utilizando su PK como filtro.
    * Actualizar un lote de registro filtrando por una condición.
    * Obtener un dato o conjunto de datos a partir de una PK.
    * Obtener todos los datos de una tabla (regularmente pequeña).
    * Obtener un dato o conjunto de datos a partir de condiciones específicas no complejas. Se entiende por condición no compleja aquella que no incluye anidación de consultas (multiples `SELECT` o `JOIN`). Normalmente estas consultas incluyen varios `AND`, `OR` e `IN`.
* Los datos devueltos pasarán por un post-procesamiento muy simple, como verificación de existencia de datos, o conversión a estructuras JSON o XML para responder a peticiones AJAX o llamadas a API (REST, SOAP).
* Los datos devueltos pasarán un post-procesamiento simple, por lo que la forma de arreglo es la mejor representación para estos.
* Los datos devueltos pasarán por un post-procesamiento complejo, por lo que la forma de instancia de modelo es la más adecuada.
* Los datos devueltos no sufrirán cambios (ediciones ni eliminaciones) en su post-procesamiento.
* El post-procesamiento es opcional. Normalmente este caso se espera un arreglo para usarse directamente.

En POO, una propiedad estática existe de manera compartida para todas las instancias de una clase, así, para muchas instancias, en memoria realmente solo existe una copia de dicha propiedad que es compartida por todas las instancias, y por tanto, cualquier instancia al cambiar el valor de una propiedad estática afecta a todas las demás instancias.

El Almacenista **solo** posee métodos estáticos, ya que carece de sentido crear instancias de un Almacenista.
El Almacenista se auxilia de [`medoo`](http://medoo.in) para crear las consultas, y para cada consulta se prepara una condición estructurada y sus correspondientes parámetros a ligar.

Las condiciones y parámetros se preparan en arreglos de php, y solo viven durante la llamada del método. Una vez devueltos los datos, la existencia del Almacenista es irrelevante, y cualquier otra consulta requiere de una nueva llamada a un método del Almacenista.

El diseño del Almacenista está hecho pensando en que el usuario trabaje con una sola operación en un momento dado.

Persistir (persist)
-------------------

Persistir es la operación básica para lidiar con un objeto Modelo. Espera una sola instancia y determina si es nueva o no para actualizar datos en la base de datos o para crear un registro nuevo.

BuscarPor (FindBy)
------------------

Los métodos buscadores esperan un nombre de Modelo con el cual trabajar, los campos que se desean devolver, ciertas condiciones y tipo de retorno, sean arreglos o modelos.