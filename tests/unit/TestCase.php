<?php
/**
 * @link https://bitbucket.org/xibalba/alpaca
 * @copyright 2014 Xibalba Lab
 * @license New BSD
 */

namespace michiq;

use \medoo as Medoo;

abstract class TestCase extends \PHPUnit_Framework_TestCase{
	protected static $_db;
	
	protected function setUp(){
		$pdoDb = 'pdo_sqlite';
		
		if(!extension_loaded('pdo') || !extension_loaded($pdoDb))
			$this->markTestSkipped('pdo and '.$pdo_database.' extension are required.');
		
		$this->mockApplication();
	}
	
	protected function mockApplication(){
		$bench = new \Ubench();
		
		$fixtureFile = __DIR__ . '/data/sqlite.sql';
		
		static::$_db = new \Medoo([
			'database_type' => 'sqlite',
			'database_file' => ':memory:'
		]);
		
		$bench->start();
		$lines = explode(';', file_get_contents($fixtureFile));
		foreach($lines as $line){
			if(trim($line) !== '') static::$_db->query($line);
		}
		$bench->end();
		
		//echo "\n\nMock data Time: ".$bench->getTime();
	}
	
	/**
	 * Assert that two array values are equal, disregarding the order.
	 *
	 * @param array $expected
	 * @param array $actual
	 * @param bool $key
	 */
	public function assertArraysEqual(array $expected, array $actual, $key = false) {
		if($key){
			ksort($actual);
			ksort($expected);
		}else{
			sort($actual);
			sort($expected);
		}
		
		$this->assertEquals($expected, $actual);
	}
}