<?php
/**
 * @link https://bitbucket.org/xibalba/alpaca
 * @copyright 2014 Xibalba Lab
 * @license New BSD
 */

namespace michiq;

use michiq\data\LlamaModel;

use xibalba\alpaca\Keeper;

class KeeperTest extends TestCase {
	
	private $__obj;
	private $__fieldDefinitions;
	private $__testValues = [
		'an_int' => 10,
		'a_real' => 10.5,
		'short_str' => 'A short string',
		'long_str' => 'A long string. A long string. A long string. A long string. A long string. A long string.'
	];
	
	protected function setUp(){
		parent::setUp();
		
		Keeper::setDbConnection(static::$_db);
		
		$this->__fieldDefinitions = require "data/fieldsDefinitions.php";
		
		$this->__obj = new LlamaModel($this->__testValues);
		Keeper::persist($this->__obj);
	}
	
	public function testPersist(){
		//Test create a new record from a model
		$newLlama = new LlamaModel();
		
		//The id must be null
		$this->assertEquals(null, $newLlama->getId());
		
		$newLlama->setValues($this->__testValues);
		Keeper::persist($newLlama);
		
		//After persist, the max pk on the table must be equal to the id of the model
		$maxIdAfterPersist = static::$_db->max(
			LlamaModel::getTableName(),
			LlamaModel::getColumnName(LlamaModel::getIdProperty())
		);
		
		$this->assertEquals($maxIdAfterPersist, $newLlama->getId());
		$this->assertFalse($newLlama->isNew());
		
		//Test modifications
		$newLlama->setValue('col_name', 'test val for random column');
		$this->assertTrue($newLlama->isDirty());
		
		Keeper::persist($newLlama);
		$this->assertFalse($newLlama->isDirty());
		
		//Test Delete. After move this part to its own method
		$this->assertTrue(Keeper::delete($newLlama));
	}
	
	public function testFindById(){
		//Test to find an existing record
		$retrivedFields = array_keys($this->__testValues);
		
		$foundedAsArray = Keeper::findById(LlamaModel::getClassName(), $this->__obj->getId(), $retrivedFields, Keeper::FETCH_ARRAY);
		$this->assertArraysEqual($this->__testValues, $foundedAsArray, true);
		
		$foundedAsModel = Keeper::findById(LlamaModel::getClassName(), $this->__obj->getId(), $retrivedFields);
		$this->assertArraysEqual($this->__testValues, $foundedAsModel->getValues($retrivedFields));
		
		//Test to find a non existing record
		$mustEmptyArray = Keeper::findById(LlamaModel::getClassName(), '-1', $retrivedFields, Keeper::FETCH_ARRAY);
		$this->assertEmpty($mustEmptyArray);
		
		$mustFalse = Keeper::findById(LlamaModel::getClassName(), '-1', $retrivedFields);
		$this->assertFalse($mustFalse);
	}
}