<?php
/**
 * @link https://bitbucket.org/xibalba/alpaca
 * @copyright 2014 Xibalba Lab
 * @license New BSD
 */

namespace michiq;

use michiq\data\LlamaModel;

class ModelTest extends TestCase {
	
	private $__obj;
	private $__fieldDefinitions;
	private $__testValues = [
		'an_int' => 10,
		'a_real' => 10.5,
		'short_str' => 'A short string',
		'long_str' => 'A long string. A long string. A long string. A long string. A long string. A long string.'
	];
	
	protected function setUp(){
		parent::setUp();
		
		$this->__fieldDefinitions = require "data/fieldsDefinitions.php";
		
		$this->__obj = new LlamaModel();
	}
	
	public function testGetColumnNames(){
		//test a single with no column_name defined
		$this->assertEquals('test.field_name', $this->__obj->getColumnName('field_name', 'test'));
		
		//Test a single with column_name defined
		$expected = 'test.'.$this->__fieldDefinitions['col_name']['column_name'];
		$this->assertEquals($expected, $this->__obj->getColumnName('col_name', 'test', false));
		
		$expected .= ' AS col_name';
		$this->assertEquals($expected, $this->__obj->getColumnName('col_name', 'test'));
	}
	
	public function testGetDefinitions(){
		//A single definition
		$expected = $this->__fieldDefinitions['long_str']['type'];
		$this->assertEquals($expected, $this->__obj->getFieldDefinition('long_str', 'type'));
		
		//Test the array definition
		$expected = $this->__fieldDefinitions['long_str'];
		$this->assertEquals($expected, $this->__obj->getFieldsDefinitions('long_str'));
	}
	
	public function testGetIdProperty(){
		$this->assertEquals('id', $this->__obj->getIdProperty());
	}
	
	public function testGetTableName(){
		$this->assertEquals('llama_models', $this->__obj->getTableName());
	}
	
	public function testValuesCycle(){
		//Initially the model is clean
		$this->assertFalse($this->__obj->isDirty());
		
		//Set values
		$this->__obj->setValues($this->__testValues);
		//Test dirty change
		$this->assertTrue($this->__obj->isDirty());
		
		//Test Get Values
		$returnedValues = $this->__obj->getValues(array_keys($this->__testValues));
		foreach($this->__testValues as $key => $val){
			$this->assertEquals($val, $returnedValues[$key]);
		}
	}
	
	
}