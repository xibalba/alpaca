<?php
/**
 * @link https://bitbucket.org/xibalba/alpaca
 * @copyright 2014 Xibalba Lab
 * @license New BSD
 */

namespace michiq\data;

use xibalba\alpaca\Model;

class LlamaModel extends Model{
	public function __construct($initialData = []){
		static::$_fields = require "fieldsDefinitions.php";
		parent::__construct($initialData);
	}
}