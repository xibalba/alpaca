DROP TABLE IF EXISTS test_data;

CREATE TABLE llama_models (
	id INTEGER PRIMARY KEY,
	an_int INTEGER,
	a_real REAL,
	short_str VARCHAR(25),
	long_str TEXT,
	field_name VARCHAR(255),
	the_column_name VARCHAR(255)
);

INSERT INTO llama_models VALUES
	(
		1,
		4324,
		6842.21,
		'foo@test.com',
		'foo@test.com',
		'field',
		'column'
	),
	(
		2,
		7645,
		24321.75,
		'bar@test.com',
		'bar@test.com',
		'field',
		'column'
	);
	
DROP TABLE IF EXISTS herd;

CREATE TABLE herd (
	id INTEGER PRIMARY KEY,
	llamas_top INTEGER,
	name VARCHAR(65),
	shepherd_name VARCHAR(128)
);

DROP TABLE IF EXISTS llama;

CREATE TABLE llama (
	id INTEGER PRIMARY KEY,
	name VARCHAR(128),
	birth DATE,
	pelage_color VARCHAR(65),
	herd_id INTEGER
);

INSERT INTO herd VALUES
	(
		1,
		12,
		'Illimani',
		'Achachic'
	),
	(
		2,
		24,
		'Tucumán',
		'Apumayta'
	);
	
INSERT INTO llama VALUES
	(
		1,
		'Chukchalu',
		'1014-01-21',
		'brown',
		1
	),
	(
		2,
		'Millmasapa',
		'1014-02-12',
		'white',
		1
	),
	(
		3,
		'Paqucha',
		'1014-02-08',
		'black',
		2
	),
	(
		4,
		'Phullusapa',
		'1014-01-06',
		'grey',
		2
	);