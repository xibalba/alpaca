<?php

return [
	'id' 		=> ['type' => 'integer'],
	'an_int' 	=> ['type' => 'integer'],
	'a_real' 	=> ['type' => 'float'],
	
	'short_str' => [
		'type' => 'string',
		'default_value' => 'A short string'
	],
	
	'long_str'  => [
		'type' => 'string',
		'default_value' => 'A long string. A long string. A long string. A long string. A long string. A long string.'
	],

	'field_name' => [
		'type' => 'string'
	],
	
	'col_name' => [
		'type' => 'string',
		'column_name' => 'the_column_name'
	]
];