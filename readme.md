Alpaca
======

Alpaca is a light [DataMapper](http://en.wikipedia.org/wiki/Datamapper) [ORM](http://es.wikipedia.org/wiki/ORM) over the [Medoo](http://medoo.in/) database framework, developed by [Yeshua Rodas](https://bitbucket.org/yybalam) and [Xibalba Lab team](https://bitbucket.org/xibalba) at [Universidad Pedagógica Nacional "Francisco Morazán"](https://bitbucket.org/upnfm/).

ORM?
----
A [ORM](http://en.wikipedia.org/wiki/Object-relational_mapping) is a very usefull tool for a painless development on Model logic on software projects.

Another ORM?
------------

Out there are two main patterns for ORM:

* [ActiveRecord](http://en.wikipedia.org/wiki/Active_record_pattern)
* [DataMapper](http://en.wikipedia.org/wiki/Data_mapper_pattern)

The major of PHP frameworks include they owns ORM, normally with ActiveRecord pattern.

By other hand, the actual popular independent ORM are huges, heavys, complex.

The main goal for Alpaca design is be light, thats why is over a tiny database framework, Medoo is just 14KB. :D